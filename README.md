# Introduction
One important concept in programming is getting input from the user. "Hello,
world!" is great, but most people will not find programs that simply run with no
interaction very useful. One way of getting input from the user is aptly named
`input()`.

`input()` is what is called a function. You can identify a function
because it will traditionally be denoted with a trailing `()`. Functions are a
way to give chunk of code a name. They can often be complex and involve
tens or hundreds of lines of code. The `input()` function will prompt the user
for input and you can utilize it as follows:
```
user_input = input('Please input a number: ')
```
The code snippet above will ask the user for a number and store whatever the
user types into the variable `user_input`. Important to note is that input
does **not** check whether or not the variable is actually a number. We'll talk
about how to deal with this problem later. For now, just be sure to input a
number at the prompt. By default, `input()` will return a *string*, **not** a
number. Because of this, we need to use to `int()` function. It will take a
string (or other type) and convert it into an *int*eger, which is simply a
nondecimal number.
```
user_number = int(user_input)
```
While `user_input` was of type `string`, `user_number` is of type `int`.

> ## Sidebar: Math in Python
> Math in Python is very simple. Addition can be done using the `+` operator,
> subtraction with `-`, multiplication with `*`, and division with `/`. There are
> a few other built in operators, but they're not important right now. What may be
> confusing to first time programmers is how the operators are used along with
> variables.
> 
> A common mistake beginner programmers make is to write something like the
> following:
> ```
> my_int = 3
> my_int + 5
> print(my_int)
> ```
> What do you think will be printed here? You might expect `8` to be printed. If so,
> you would be surprised when `3` was printed instead. That is because line 2 does
> not work as you might expect. Doing `my_int + 5` **did** in fact do `3 + 5`;
> however, the `8` was not stored into any variable! Python simply ran the
> calculation and threw away the result. The correct code is below.
> ```
> my_int = 3
> my_int = my_int + 5
> print(my_int)
> # prints 8
> ```
> This might look strange; after all, how could `my_int` be equal to `my_int + 3`?
> What you must remember is that `=` in Python is for *assignment*. It does not
> represent *equality*. We will learn how to check equality later on in this
> tutorial. The line `my_int = my_int + 5` first evaluates the **right** side - 
> Python does `3 + 5` - and *then* will set the result - `8` - into `my_int`.
> Because this pattern of accessing a variable and the storing the value back into
> the same variables is common, Python offers a shorthand.
> ```
> my_int = 3
> my_int += 5
> print(my_int)
> # prints 8
> ```
> This will do the same thing as the second example above.
# Task
> Note: you may need to check back to older tutorials to complete this task. It's
> important that you recall past material. From now on, assume that anything
> covered in a previous tutorial might be needed later.

Write a program that asks the user for 2 numbers, and then adds them together
and prints the result. The program output should look something like this:
```
Type a number: 5
Type a number: 2
7
```
